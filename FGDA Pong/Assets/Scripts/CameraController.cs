﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//basic camera script
public class CameraController : MonoBehaviour
{
    public GameObject player;

    private Vector3 offset;

    void Start()
    {
        offset = transform.position - player.transform.position;
    }

    void LateUpdate()
    {
        //currently following player
        //may need to adjust or tweak
        // could do a lerp camera function to give the camera a little delay when following, 
        //might make the following feel more natural?
        transform.position = player.transform.position + offset;
    }
}
