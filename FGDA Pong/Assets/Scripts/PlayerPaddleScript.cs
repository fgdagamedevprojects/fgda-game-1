﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//Main player script
//currently movement controls are wsad, but there's a strange delay that doesn't feel natural for a pong paddle, need to resolve
//paddle rotation is the mousewheel. It's not that smooth though.
//want to change rotation to follow the mouse probably

public class PlayerPaddleScript : MonoBehaviour
{
    public float playerMoveSpeed;
    public float playerRotateSpeed;
    float translationX;
    float translationZ;
    float rotationZ;

    Ray mousePointingRay;
    RaycastHit[] theMouseRayHit;

    private Camera theMainCamera;

    Vector3 targetDirection;
    

    // Start is called before the first frame update
    void Start()
    {
        theMainCamera = Camera.main;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        mousePointingRay = theMainCamera.ScreenPointToRay(Input.mousePosition);
        
        //setup the translations to wsad
        translationX = Input.GetAxis("Horizontal") * playerMoveSpeed;
        translationZ = Input.GetAxis("Vertical") * playerMoveSpeed;


        theMouseRayHit = Physics.RaycastAll(mousePointingRay, Mathf.Infinity, LayerMask.GetMask("MouseRaycast"));

        if(theMouseRayHit[0].collider != null)
        {
            targetDirection = theMouseRayHit[0].point - transform.position;
            
            targetDirection.y = 0f;

            transform.rotation = Quaternion.LookRotation(targetDirection, transform.up);
            
        }

        // uncomment this line to use mousewheel for rotation
        //rotationZ = Input.GetAxis("Mouse ScrollWheel") * playerRotateSpeed;


        //without this the translations are huge. This scales to the timescale of the FixedUpdate function
        translationZ *= Time.deltaTime;
        translationX *= Time.deltaTime;
        

        //translate and rotate the player object.
        transform.Translate(translationX, 0, translationZ, Space.World);

        //rotate around the y axis, the center of the player paddle
        //transform.Rotate(0f, -rotationZ, 0f, Space.Self);

    }
}
