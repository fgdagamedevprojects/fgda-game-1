﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//this script gets referenced by GreenBallScript to increase the score if a green ball hits the player
//and by GoalKeeperScript to decrease the score if a red ball hits the goal. 
public class ScoreKeeper : MonoBehaviour
{

    public int theScore;
    GoalKeeperScript theScoreBlockToChange;
    public int scorePrevious;

    GoalKeeperScript[] goalScoreKeepers;
    
    void Start()
    {
        theScore = 5;  //starting score. 
        scorePrevious = theScore;

        

        //this pulls the game objects for the score keeper blocks. there are 10 score keeper blocks that indicate the score from 0-10. 
        // goalscorekeepers 0 is the 10% block. goalscorekeepers[9] is the 100% block
        goalScoreKeepers = new GoalKeeperScript[10];

        //cache all the scorekeeper blocks 
        goalScoreKeepers[0] = GameObject.Find("GoalScoreKeeper10").GetComponent<GoalKeeperScript>();
        goalScoreKeepers[1] = GameObject.Find("GoalScoreKeeper20").GetComponent<GoalKeeperScript>();
        goalScoreKeepers[2] = GameObject.Find("GoalScoreKeeper30").GetComponent<GoalKeeperScript>();
        goalScoreKeepers[3] = GameObject.Find("GoalScoreKeeper40").GetComponent<GoalKeeperScript>();
        goalScoreKeepers[4] = GameObject.Find("GoalScoreKeeper50").GetComponent<GoalKeeperScript>();
        goalScoreKeepers[5] = GameObject.Find("GoalScoreKeeper60").GetComponent<GoalKeeperScript>();
        goalScoreKeepers[6] = GameObject.Find("GoalScoreKeeper70").GetComponent<GoalKeeperScript>();
        goalScoreKeepers[7] = GameObject.Find("GoalScoreKeeper80").GetComponent<GoalKeeperScript>();
        goalScoreKeepers[8] = GameObject.Find("GoalScoreKeeper90").GetComponent<GoalKeeperScript>();
        goalScoreKeepers[9] = GameObject.Find("GoalScoreKeeper100").GetComponent<GoalKeeperScript>();

        //set the scorekeeper blocks to red if the score is less than the corresponding block.
        for (int x = theScore; x < 10; x++)
        {
            goalScoreKeepers[x].isRed = true;
        }
        

    }

    void Update()
    {
        if(theScore <= 0)
        {
            //game over
        }

        if(theScore >= 10)
        {
            //win level
        }


    }

 

    public void UpdateScore()
    {

        //if the score decreases, change the score block to red
        if (theScore < scorePrevious)
        {
            goalScoreKeepers[theScore].isRed = true;
           
        }

        //if the score increases change the score block to green
        if (theScore > scorePrevious)
        {
            goalScoreKeepers[theScore - 1].isRed = false;  //score - 1 pulls the proper index for the green block
           
        }
        

    }


}
