﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Main script for the 10x blocks that show the score. Each block has an instance of this class going once the game loads, each block is
//mapped to 10% - 100% of the total score of 10 points.
//this setup could be adapted to have greater scores than 100
//Could also use more blocks than 10 for greater resolution on the score
public class GoalKeeperScript : MonoBehaviour
{
    public bool isRed;
    ScoreKeeper mainScore;
    
    // Start is called before the first frame update
    void Start()
    {

        mainScore = GameObject.Find("Goal").GetComponent<ScoreKeeper>();


    }

    // Update is called once per frame
    void Update()
    {

        //change the color depending on the isRed boolean
        // this variable is set by ScoreKeeper during updateScore depending on the score.
        //could also use any custom color, not just the Color.red or Color.green
        // needs to be updated once graphics exist to not rely on the material color, but probably change the graphic or color of the graphic.
        if (isRed)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.red;
        }

        else
        {
            gameObject.GetComponent<Renderer>().material.color = Color.green;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "RedBall")
        {
            Destroy(collision.transform.gameObject);
            mainScore.scorePrevious = mainScore.theScore;
            mainScore.theScore--;
            mainScore.UpdateScore();
            
        }


    }
}
