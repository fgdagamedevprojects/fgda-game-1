﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Randomly spawns green and red balls for testing
public class RandomSpawnScript : MonoBehaviour
{
    public GameObject redBall;
    public GameObject greenBall;

    //change these to spawn more than 1 ball during the spawining interval.
    int startingRedBallNumber = 1;
    int startingGreenBallNumber = 1;

    GameObject redBallInstance;
    TimerClass redBallSpawnTimer;
    public float redSpawnInterval;
    float redForceMin = 100f;
    float redForceMax = 300f;

    GameObject greenBallInstance;
    TimerClass greenBallSpawnTimer;
    public float greenSpawnInterval;
    float greenForceMin = 100f;
    float greenForceMax = 300f;

    float spawnLocationMin = -50f;
    float spawnLocationMax = 50f;


    void Start()
    {
        //how fast the red and green balls spawn in seconds
        redSpawnInterval = 2f;
        greenSpawnInterval = 2f;
        redBallSpawnTimer = new TimerClass();
        greenBallSpawnTimer = new TimerClass();

        redBallSpawnTimer.StartTimer();
        greenBallSpawnTimer.StartTimer();

    }

    // Update is called once per frame
    void Update()
    {
        redBallSpawnTimer.UpdateTimer();
        if (redBallSpawnTimer.GetTime() > redSpawnInterval)
        {
            SpawnRedBalls();
        }

        greenBallSpawnTimer.UpdateTimer();
        if (greenBallSpawnTimer.GetTime() > greenSpawnInterval)
        {
            SpawnGreenBalls();
        }


    }

    void SpawnRedBalls()
    {
        Vector3 startingPosition;
        Vector3 forceDirection;
        float redForce;

        redForce = Random.Range(redForceMin, redForceMax);
        startingPosition.x = Random.Range(spawnLocationMin, spawnLocationMax);
        startingPosition.z = Random.Range(spawnLocationMin, spawnLocationMax);
        startingPosition.y = .5f;

        forceDirection.x = Random.Range(0f, 1f) * redForce;
        forceDirection.z = Random.Range(0f, 1f) * redForce;
        forceDirection.y = 0f;



        for (int x = 0; x < startingRedBallNumber; x++)
        {

            redBallInstance = Instantiate(redBall);
            redBallInstance.transform.position = startingPosition;
            redBallInstance.GetComponent<Rigidbody>().AddForce(forceDirection);
            redBallSpawnTimer.ResetTimer();
        }

    }

    void SpawnGreenBalls()
    {


        Vector3 startingPosition;
        Vector3 forceDirection;
        float greenForce;

        greenForce = Random.Range(greenForceMin, greenForceMax);
        startingPosition.x = Random.Range(spawnLocationMin, spawnLocationMax);
        startingPosition.z = Random.Range(spawnLocationMin, spawnLocationMax);
        startingPosition.y = .5f;

        forceDirection.x = Random.Range(0f, 1f)*greenForce;
        forceDirection.z = Random.Range(0f, 1f)*greenForce;
        forceDirection.y = 0f;



        for (int x = 0; x < startingGreenBallNumber; x++)
        {

            greenBallInstance = Instantiate(greenBall);
            greenBallInstance.transform.position = startingPosition;
            greenBallInstance.GetComponent<Rigidbody>().AddForce(forceDirection);
            greenBallSpawnTimer.ResetTimer();
        }


    }

}
