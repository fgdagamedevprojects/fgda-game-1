﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//basic timer utility class
public class TimerClass
{
    public bool isTimerRunning = false;
    private float timeElapsed = 0.0f;
    private float currentTime = 0.0f;
    private float lastTime = 0.0f;
    private float timeScaleFactor = 1.0f;

    //function updates the timer with the current time since the start timer function was run.
    //needs to be run during the main Update function to create a timer that checks if a certain time has gone by
    //can also use to keep track of time elapsed by just running updateTimer when needed.
    public void UpdateTimer()
    {
        timeElapsed = Mathf.Abs(Time.realtimeSinceStartup - lastTime);

        if (isTimerRunning)
        {
            currentTime += timeElapsed * timeScaleFactor;
        }

        lastTime = Time.realtimeSinceStartup;

    }

    //needs to be run before UpdateTimer is called
    public void StartTimer()
    {
        isTimerRunning = true;
        lastTime = Time.realtimeSinceStartup;
    }


    public void StopTimer()
    {
        isTimerRunning = false;
    }

    //Reset timer resets the time, but keeps the timer running
    public void ResetTimer()
    {
        lastTime = Time.realtimeSinceStartup;
        currentTime = 0f;
    }

    //returns the time
    public float GetTime()
    {
        return currentTime;
    }
}
