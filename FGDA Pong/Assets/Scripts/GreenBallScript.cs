﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//keeps track of if a green ball hits the player and updates the score by updating the ScoreKeeper class instance.
public class GreenBallScript : MonoBehaviour
{
    int theScore;
    int scorePrevious;
    ScoreKeeper mainScoreKeeper;
    
    // Start is called before the first frame update
    void Start()
    {
        mainScoreKeeper = GameObject.Find("Goal").GetComponent<ScoreKeeper>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.transform.tag == "Player")
        {
            mainScoreKeeper.scorePrevious = mainScoreKeeper.theScore;
            mainScoreKeeper.theScore++;
            mainScoreKeeper.UpdateScore();

            Destroy(gameObject);
        }
    }
}
